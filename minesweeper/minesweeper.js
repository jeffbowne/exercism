export const annotate = (input) => {

  let result = Array.from(input);

  for (let l = 0; l < input.length; l++) {

    for (let s = 0; s < input[l].length; s++) {
      if (input[l][s] === '*') { continue; }

      let count = 0;

      if (input[l][s + 1] === '*') { count++; }
      if (input[l][s - 1] === '*') { count++; }

      if (l !== 0) {
        if (input[l - 1][s + 1] === '*') { count++; }
        if (input[l - 1][s] === '*') { count++; }
        if (input[l - 1][s - 1] === '*') { count++; }
      }

      if (l < (input.length - 1)) {
        if (input[l + 1][s - 1] === '*') { count++; }
        if (input[l + 1][s] === '*') { count++; }
        if (input[l + 1][s + 1] === '*') { count++; }
      }

      if (count > 0) {
        let a = result[l].split("");
        a[s] = count;
        result[l] = a.join("");
      }
    }
  }

  return result;
}

// for testing
// annotate(['    ', '   *', ' *  ', '*   ', '  * ', '    ']);
