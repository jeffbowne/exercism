const codonIndex = {
  AUG: 'Methionine',
  UUU: 'Phenylalanine',
  UUC: 'Phenylalanine',
  UUA: 'Leucine',
  UUG: 'Leucine',
  UCU: 'Serine',
  UCC: 'Serine',
  UCA: 'Serine',
  UCG: 'Serine',
  UAU: 'Tyrosine',
  UAC: 'Tyrosine',
  UGU: 'Cysteine',
  UGC: 'Cysteine',
  UGG: 'Tryptophan',
  UAA: 'stop',
  UAG: 'stop',
  UGA: 'stop',
};

export const translate = (rna) => {
  let proteins = [];
  if (!rna){ return proteins; }
  for(let i=0 ; i < rna.length; i+=3) {
    let codon = rna.slice(i, i+3);
    if (!codonIndex[codon]){
      throw 'Invalid codon';
    } else if (codonIndex[codon] === 'stop'){
      return proteins;
    }
    else {
      proteins.push(codonIndex[codon]);
    }
  }
  return proteins;
};
