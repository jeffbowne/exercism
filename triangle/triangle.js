//
// This is only a SKELETON file for the 'Triangle' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export class Triangle {
  constructor(a, b, c) {
    this.sideA = a;
    this.sideB = b;
    this.sideC = c;

    this.isValid = this.sideA + this.sideB > this.sideC
                && this.sideA + this.sideC > this.sideB
                && this.sideB + this.sideC > this.sideA;
  }

  get isEquilateral() { // all equal sides
    return this.isValid
      && this.sideA === this.sideB
      && this.sideA === this.sideC;
  }

  get isIsosceles() { // at least two equal sides
    return this.isValid
      && (this.sideA === this.sideB || this.sideA === this.sideC || this.sideB === this.sideC);
  }

  get isScalene() { // all sides different
    return this.isValid
      && this.sideA !== this.sideB
      && this.sideA !== this.sideC
      && this.sideB !== this.sideC;
  }
}